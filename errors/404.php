<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php require("../blocks/settings.php") ?>
        <title>Error 404</title>
    </head>

    <body>
        <div class="wrapper">
            <?php require("../blocks/header.php") ?>
            <main class="main">
                <h1 style="text-align: center;">Error 404</h1>
                <p style="text-align: center;">
                    Кажется, по такому адресу страницы нет
                </p>
            </main>
            <?php require("../blocks/footer.php") ?>
        </div>
    </body>

</html>