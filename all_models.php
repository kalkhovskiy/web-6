<?php

session_name('auth');
session_start();

    require_once 'database/connect.php';

    $avto = mysqli_query($connect, "SELECT * FROM `avto`");
    $avto = mysqli_fetch_all($avto);
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php require("blocks/settings.php") ?>
        <title>Все модели</title>
    </head>

    <body>
        <div class="wrapper">
            <?php require("blocks/header.php") ?>
            <main class="main">
            <h1 style="text-align: center;">Список доступных моделей</h1>  
                <?php require("blocks/database-table.php") ?>

<?php if ($_SESSION['auth']['admin']):  ?>
<table>
	<tr>
                <td><a href="add.php" class="button button1">Добавить автомобиль</a></td>
	</tr>
</table>
<?php endif; ?>
                                                                               
            </main>
            <?php require("blocks/footer.php") ?>
        </div>
    </body>

</html>
