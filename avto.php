<?php
session_name('auth');
session_start();

    require_once 'database/connect.php';

    $avto_id = $_GET['id'];

    $avto = mysqli_query($connect, "SELECT * FROM `avto` WHERE `id` = '$avto_id'");

    $avto = mysqli_fetch_assoc($avto);

?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php require("blocks/settings.php") ?>
        <title><?= $avto['model'] ?></title>
    </head>

    <body>
        <div class="wrapper">
            <?php require("blocks/header.php") ?>
            <main class="main">
                <div class="container">
                    <h1>Модель: <?= $avto['model'] ?></h1>
                    <img class="img_car_big" src=<?= $avto['img'] ?> alt="Картинка потерялась">
                    <h2>Производитель: <?= $avto['producer'] ?></h2>
                    <h2>Год выпуска: <?= $avto['year'] ?></h2>
                    <p>Описание: <?= $avto['description'] ?></p>
<table>
<tr>
<td><a onclick="javascript:history.back(); return false;" class="button button1">Назад</a></td>

<?php if ($_SESSION['auth']['admin']):  ?>
	
	<td><a href="update.php?id=<?= $avto['id'] ?>" class="button button1">Изменить</a></td>
	<td><a class="button button1" href="database/delete.php?id=<?= $avto['id'] ?>">Удалить</a></td>

<?php endif; ?>

</tr>
</table>

                </div>
            </main>
            <?php require("blocks/footer.php") ?>
        </div>
    </body>

</html>