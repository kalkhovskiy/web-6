<?php
    require_once 'database/connect.php';

    $avto = mysqli_query($connect, "SELECT * FROM `avto` ORDER BY RAND() LIMIT 3;");
    $avto = mysqli_fetch_all($avto);
    mysql_close($connect);
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php require("blocks/settings.php") ?>
        <title>AvtoCatalog</title>
    </head>

    <body>
        <div class="wrapper">
            <?php require("blocks/header.php") ?>
            <main class="main">
                <h1 style="text-align: center;">Каталог автомобилей</h1>
                <p>
                    Добро пожаловать в веб приложение, представляющее собой каталог автомобилей.
                </p>
                <p>
                    Возможно вас заинтересует:
                </p>
                <?php require("blocks/database-table.php") ?>
                <div style="text-align: center;">
                    <a href="all_models.php" class="button button1">Список всех моделей</a>
                </div>
            </main>
            <?php require("blocks/footer.php") ?>
        </div>
    </body>

</html>