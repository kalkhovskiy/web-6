<?php

session_name('auth');
session_start();
if (!$_SESSION['auth']['admin']){
header("Location: admin/admin.php");
}

    require_once 'database/connect.php';

    $avto_id = $_GET['id'];

    $avto = mysqli_query($connect, "SELECT * FROM `avto` WHERE `id` = '$avto_id'");

    $avto = mysqli_fetch_assoc($avto);

?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php require("blocks/settings.php") ?>
        <title><?= $avto['model'] ?> изменение</title>
    </head>

    <body>
        <div class="wrapper">
            <?php require("blocks/header.php") ?>
            <main class="main">
            
                <h1>AvtoCatalog</h1>
                <h2>Изменить запись в базе данных</h2>
                <div class="container">
                <form action="database/update.php" method="post">
                    <input type="hidden" id="id" name="id" value="<?= $avto['id'] ?>">
                    <div class="row">
                        <div class="col-20">
                            <label for="model">Модель</label>
                        </div>
                        <div class="col-80">
                            <input type="text" id="model" name="model" placeholder="Модель" value="<?= $avto['model'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-20">
                            <label for="producer">Производитель</label>
                        </div>
                        <div class="col-80">
                            <input type="text" id="producer" name="producer" placeholder="Производитель" value="<?= $avto['producer'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-20">
                            <label for="year">Год выпуска</label>
                        </div>
                        <div class="col-80">
                            <input type="text" id="year" name="year" placeholder="Год выпуска" value="<?= $avto['year'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-20">
                            <label for="img">Ссылка на изображение</label>
                        </div>
                        <div class="col-80">
                            <input type="text" id="img" name="img" placeholder="Ссылка на изображение" value="<?= $avto['img'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-20">
                            <label for="description">Описание</label>
                        </div>
                        <div class="col-80">
                            <textarea id="description" name="description" placeholder="Описание" style="height:10vw"><?= $avto['description'] ?></textarea>
                        </div>
                    </div>
                    <div class="row">
<table>
<tr>
	<td><a onclick="javascript:history.back(); return false;" class="button button1">Назад</a></td>
	<td><button type="submit" class="button button1">Изменить</button> </td>
</tr>
</table>

                    </div>
                </form>
            </div>

            </main>
            <?php require("blocks/footer.php") ?>
        </div>
    </body>

</html>