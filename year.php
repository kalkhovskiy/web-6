<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php require("blocks/settings.php") ?>
        <title>AvtoCatalog</title>
    </head>

    <body>
        <div class="wrapper">
            <?php require("blocks/header.php") ?>
            <main class="main">
                <div class="container">
                    <h1>Введите необходимый период</h1>
                    <form action="year-avto.php" method="post">
                        <div class="row">
                            <div class="col-20">
                                <label for="year_start">Найти с</label>
                            </div>
                            <div class="col-80">
                                <input type="text" id="year_start" name="year_start" placeholder="Начало">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-20">
                                <label for="year_end">По</label>
                            </div>
                            <div class="col-80">
                                <input type="text" id = "year_end" name="year_end" placeholder="Конец">
                            </div>
                        </div>
                        <div class="row">
                            <button type="submit" class="button button1">Найти</button>
                        </div>
                    </form>
                </div>
            </main>
            <?php require("blocks/footer.php") ?>
        </div>
    </body>

</html>