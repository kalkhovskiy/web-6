<?php
session_name('auth');
session_start();
if (!$_SESSION['auth']['admin']){
header("Location: admin/admin.php");
}
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php require("blocks/settings.php") ?>
        <title>Добавление автомобиля</title>
    </head>
    <body>
        <div class="wrapper">
            <?php require("blocks/header.php") ?>
            <main class="main">   
                <div class="container">
                <h1>Добавление автомобиля</h1>
                <form action="database/create.php" method="post">
                    <div class="row">
                        <div class="col-20">
                            <label for="model">Модель</label>
                        </div>
                        <div class="col-80">
                            <input type="text" id="model" name="model" placeholder="Модель">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-20">
                            <label for="producer">Производитель</label>
                        </div>
                        <div class="col-80">
                            <input type="text" id = "producer" name="producer" placeholder="Производитель">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-20">
                            <label for="year">Год выпуска</label>
                        </div>
                        <div class="col-80">
                            <input type="text" id="year" name="year" placeholder="Год выпуска">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-20">
                            <label for="img">Ссылка на изображение</label>
                        </div>
                        <div class="col-80">
                            <input type="text" id="img" name="img" placeholder="Ссылка на изображение">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-20">
                            <label for="description">Описание</label>
                        </div>
                        <div class="col-80">
                        <textarea name="description" id="description" placeholder="Описание" style="height:10vw"></textarea>
                        </div>
                    </div>
                    <div class="row">
<table>
<tr>
	<td> <a onclick="javascript:history.back(); return false;" class="button button1">Назад</a></td>
	<td> <button type="submit" class="button button1">Добавить автомобиль</button></td>
</tr>
</table

                    </div>
                </form>
            </div>
            </main>
            <?php require("blocks/footer.php") ?>
        </div>
    </body>

    
    
</html>