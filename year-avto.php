<?php
    require_once 'database/connect.php'; 

    if(empty($_POST['year_start'])){
        $year_start = '0';
    }
    else{
        $year_start = strip_tags(htmlspecialchars($_POST['year_start']));
    }

    if (empty($_POST['year_end'])) {
        $year_end = '9999';
    }
    else{
        $year_end = strip_tags(htmlspecialchars($_POST['year_end']));
    }

    $avto = mysqli_query($connect, "SELECT * FROM `avto` WHERE year between '$year_start' and '$year_end'");
    $avto = mysqli_fetch_all($avto);
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php require("blocks/settings.php") ?>
        <title><?= $year_start.'-'.$year_end ?></title>
    </head>

    <body>
        <div class="wrapper">
            <?php require("blocks/header.php") ?>
            <main class="main">
                <?php
                    if(count($avto) == 0){
                        echo "<p>Ничего не найдено</p>";
                    }
                    else{
                        require("blocks/database-table.php");
                    }
                ?>
            </main>
            <?php require("blocks/footer.php") ?>
        </div>
    </body>

</html>