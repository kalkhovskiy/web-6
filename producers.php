<?php
    require_once 'database/connect.php';

    $producer = mysqli_query($connect, "SELECT DISTINCT producer FROM `avto`");
    $producer = mysqli_fetch_all($producer);
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php require("blocks/settings.php") ?>
        <title>Производители</title>
    </head>

    <body>
        <div class="wrapper">
            <?php require("blocks/header.php") ?>
            <main class="main">
            <h1 style="text-align: center;">Список доступных производителей</h1>    
            <div style="overflow-x:auto;">
                <table>
                    <tr>
                        <th>Производитель</th>
                        <th>Модели производителя</th>
                    </tr>
                    <?php
                        foreach ($producer as $producer) 
                        {   ?>
                                <tr>
                                    <td><?= $producer[0] ?></td>
                                    <td>
                                        <a href="producer-avto.php?id=<?= $producer[0] ?>" class="button button1">Модели</a>
                                    </td>
                                </tr>
                            <?php
                        }
                    ?>
                </table>
            </div>

            </main>
            <?php require("blocks/footer.php") ?>
        </div>
    </body>

</html>